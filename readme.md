# HBPull

hbpull connects to a cloud hawkbit instance and pulls artifacts into a local hawkbit instance. 


## Configure
hbpull

    git pull https://gitlab.com/smartfactory-owl/hbpull
    cd hbloader
    rename hblcfg_sample.json to hblcfg.json 
    modify hblcfg.json (tenant_id, login, password)
    sudo pip3 install -r requirements.txt
    sudo python3 hbpull.py
    deploy an app via rollout management gui

    The configuration can be changed via the supplied web-gui http://localhost:8081/config


License: LGPLv2.1

Copyright
---------
    Copyright (C) Patrick Heidemann
    Copyright (C) Additional code Eugene Nuribekov, Jan Alsters

Software based on rauc-hawkbit
https://github.com/rauc/rauc-hawkbit

    Copyright (C) 2016-2020 Pengutronix, Enrico Joerns <entwicklung@pengutronix.de>
    Copyright (C) 2016-2020 Pengutronix, Bastian Stender <entwicklung@pengutronix.de>
    
    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.
    
    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA



