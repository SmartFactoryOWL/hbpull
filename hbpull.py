#! /usr/bin/env python3

import sys
import os
import asyncio
import aiohttp
import json
import re
import pathlib
import logging
import random
from getpass import getpass
from lib.hbclient import HBClient
from lib.ddi.client import DDIClient
from lib.ddi.client import (ConfigStatusExecution, ConfigStatusResult)
from hbpull_gui.gui import Hbpull_gui
from aiohttp import web

HBLCFG = 'hblcfg.json'
hblcfg_path = ''
cached_stamp = 0

def result_callback(result):
    print("Result:   {}".format('SUCCESSFUL' if result == 0 else 'FAILED' ))

def step_callback(percentage, message):
    print("Progress: {:>3}% - {}".format(percentage, message))

def main():
    #save last updated timestamp to CACHED_STAMP
    hblcfg_path = pathlib.Path(HBLCFG)
    if hblcfg_path.exists():
        #get absolute path
        hblcfg_path = hblcfg_path.resolve()
    global cached_stamp
    cached_stamp = os.stat(HBLCFG).st_mtime
    
    #create watchdog task
    loop = asyncio.get_event_loop()
    loop.create_task(config_watchdog())

    #create task for gui
    loop.create_task(hbpull_gui())

    while True:
        #create maintask
        maintask = loop.create_task(_hbpull())
        loop.run_forever()
        maintask.cancel()

async def _hbpull():
    config = load_config()
    #logfmt= '%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d-%(funcName)s] %(message)s'
    logfmt = '%(asctime)s [%(levelname)s] %(message)s'
    datefmt= '%Y-%m-%d %H:%M:%S'

    logging_lvl = config['loglevel']
    if logging_lvl == "INFO":
        log_lvl = logging.INFO
    elif logging_lvl == "DEBUG":
        log_lvl = logging.DEBUG
    elif logging_lvl == "WARNING":
        log_lvl = logging.WARNING
    elif logging_lvl == "ERROR":
        log_lvl = logging.ERROR
    else:
        log_lvl = logging.DEBUG

    logging.basicConfig(filename='hbpull.log', encoding='utf-8', filemode='a', level=log_lvl, format=logfmt, datefmt=datefmt)
    logging.getLogger().addHandler(logging.StreamHandler())

    async with aiohttp.ClientSession() as session:
        client = HBClient(session, result_callback, step_callback, **config)

        await client.run_ddi()

        await client.start_polling()

async def config_watchdog():
    global cached_stamp
    loop = asyncio.get_event_loop()
    while True:
        stamp = os.stat(HBLCFG).st_mtime
        if stamp != cached_stamp:
            cached_stamp = stamp
            print("New Config was found! Restarting hbpull...")
            loop.stop()
        await asyncio.sleep(10)

async def hbpull_gui():
    #start webserver
    gui = Hbpull_gui()
    app = await gui.get_app()
    runner = web.AppRunner(app, access_log=None)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', 8081)
    await site.start()

    #await asyncio.Event().wait()

def ask_parameters(config):
    '''
    Running first time
    '''

    ''' get host ip '''
    ip = input('Enter IP address (default 127.0.0.1): ')

    if not ip:
        ip = '127.0.0.1'

    config['ip'] = ip

    ''' get host port '''
    while True:
        port = input ('Enter port (default 443): ')
            
        if not port:
            port = '443'

        if port.isdecimal():
            p = int(port)
            if (p in range(1023, 65535)) or (p in (80, 443)):
                break 

        print('Invalid port number')

    config['port'] = port

    ''' target name '''
    while True:
        target_name = input('Enter device name (for humans): ')
        if target_name:
            break

    config['target_name'] = target_name

    ''' controller_id  '''
    while True:
        controller_id = input('Enter controller ID (for computers): ')
        if controller_id:
            break

    config['controller_id'] = controller_id

    ''' get tenant id '''
    tenant_id = input('Enter tenant id: ')

    if not tenant_id:
        tenant_id = 'default'

    config['tenant_id'] = tenant_id

    ''' management login '''
    login = input('Login: ')
    if not login:
        login = 'admin'

    config['login'] = login

    config['password'] = getpass('Password: ')

    ''' SSH mode '''
    while True:
        yn = input('SSL mode (y/n): ')

        if yn in ('y', 'Y', ''):
            ssl  = 'True'
            break

        if yn in ('n', 'N'):
            ssl = 'False'
            break

        print('Wrong input')

    config['ssl'] = ssl
    
    '''run_as_service'''
    while True:
        yn = input('Run installed as service ? (Yes/No/Ask)')

        if yn in ('y', 'Y', ''):
            run_as_service  = 'yes'
            break

        if yn in ('n', 'N'):
            run_as_service = 'no'
            break

        if yn in ('a', 'A'):
            run_as_service = 'ask'
            break

        print('Wrong input')

    config['run_as_service'] = run_as_service

def get_envs(config):
    try:
        ip = os.environ['IP_ADDRESS']
        print("[ENV] IP_ADDRESS: {}".format(ip))
    except:
        ip = '127.0.0.1'
        print("No ENV 'IP_ADDRESS' found! Using default: {}".format(ip))
    config['ip'] = ip
    
    try:
        port = os.environ['PORT']
        p = int(port)
        if (p in range(1023, 65535)) or (p in (80, 443)):
            port = str(p)
        else:
            port = '443'
        print("[ENV] PORT: {}".format(port))
    except:
        port = '443'
        print("No ENV 'PORT' found! Using default: {}".format(port))
    config['port'] = port

    try:
        target_name = os.environ['TARGET_NAME']
        print("[ENV] TARGET_NAME: {}".format(target_name))
    except:
        target_name = 'default_target'
        print("No ENV 'TARGET_NAME' found! Using default: {}".format(target_name))
    config['target_name'] = target_name

    try:
        controller_id = os.environ['CONTROLLER_ID']
        print("[ENV] CONTROLLER_ID: {}".format(controller_id))
    except:
        controller_id = random.randint(100000,1000000)
        print("No ENV 'CONTROLLER_ID' found! Using random id: {}".format(controller_id))
    config['controller_id'] = controller_id

    try:
        tenant_id = os.environ['TENANT_ID']
        print("[ENV] TENANT_ID: {}".format(tenant_id))
    except:
        tenant_id = 'default'
        print("No ENV 'TENANT_ID' found! Using default: {}".format(tenant_id))
    config['tenant_id'] = tenant_id

    try:
        user = os.environ['USER']
        print("[ENV] USER: {}".format(user))
    except:
        user = 'admin'
        print("No ENV 'USER' found! Using default: {}".format(user))
    config['login'] = user

    try:
        passwd = os.environ['PASSWORD']
        print("[ENV] PASSWORD: {}".format(passwd))
    except:
        passwd = 'admin'
        print("No ENV 'PASSWORD' found! Using default: {}".format(passwd))
    config['password'] = passwd

    try:
        ssl = os.environ['SSL']
        print("[ENV] SSL: {}".format(ssl))
    except:
        ssl = True
        print("No ENV 'SSL' found! Using default: {}".format(ssl))
    config['ssl'] = ssl

    try:
        run_as_service = os.environ['RUN_AS_SERVICE']
        print("[ENV] RUN_AS_SERVICE: {}".format(run_as_service))
    except:
        run_as_service = 'no'
        print("No ENV 'RUN_AS_SERVICE' found! Using default: {}".format(run_as_service))
    config['run_as_service'] = run_as_service

    try:
        loglvl = os.environ['LOG_LEVEL']
        print("[ENV] LOG_LEVEL: {}".format(loglvl))
    except:
        loglvl = 'INFO'
        print("No ENV 'LOG_LEVEL' found! Using default: {}".format(loglvl))
    config['loglevel'] = loglvl

    try:
        docker_registry = os.environ['DOCKER_REGISTRY']
        print("[ENV] DOCKER_REGISTRY: {}".format(docker_registry))
    except:
        print("No ENV 'DOCKER_REGISTRY' found. Using default registry.")
        docker_registry = 'default'
    config['docker_registry'] = docker_registry

def load_config():
    '''
    Load configuration params
    If file exists load it as params,
    if not create it with default parameters
    '''
    config = {
    "ssl" : False,
    "host" :"127.0.0.1",
    "tenant_id" : "default",
    "target_name" : "",
    "login" : "admin",
    "password" : "admin",
    "auth_token" : "",
    "attributes" : {"MAC": ""},
    "loglevel" : "INFO",
    "run_as_service" : "no",
    "port" :  "443",
    "docker_registry": "default"
    }

    ''' 
    if config file exist and contains host ip 
    return this config 
    '''
    if pathlib.Path(HBLCFG).exists():
        with open(HBLCFG, "r") as config_file:
            config = json.load(config_file)

        if config["ip"]:
            return config

    '''
    else ask to input parameters manually
    save and rerun updated config
    '''
    get_envs(config)
    #ask_parameters(config)

    with open(HBLCFG, "w") as config_file:
            json.dump(config, config_file, indent=4)

    return config

if __name__ == "__main__":
    main()
