import asyncio
import json
import tarfile
from pathlib import Path
import subprocess
import re
import socket
import os
import io
from aiohttp.client_exceptions import ClientOSError, ClientResponseError
from datetime import datetime, timedelta
import aiohttp
from aiohttp.client import ClientTimeout
from aiohttp import MultipartWriter
from .ddi.client import DDIClient, APIError
from .ddi.client import (ConfigStatusExecution, ConfigStatusResult)
from .ddi.deployment_base import (
    DeploymentStatusExecution, DeploymentStatusResult)
from .ddi.cancel_action import (
    CancelStatusExecution, CancelStatusResult)
from .mi.client import MIClient
import logging
from requests_toolbelt.multipart.encoder import MultipartEncoder

class HBClient(object):
    """
    HawkBit downloader
    """

    def __init__(self,
                 session,
                 result_callback,
                 step_callback=None,
                 lock_keeper=None,
                 **kwargs):

        super(HBClient, self).__init__()

        self.logger = logging.getLogger('hbpull')
        self.session = session
        self.timeout = 10
        self.config = kwargs

        self.logger.debug(self.config)
        self.attributes = kwargs['attributes']
        self.ssl = kwargs['ssl']
        self.ehb_ssl = kwargs['edgehb_ssl']
        self.host = '{}:{}'.format(kwargs['ip'],kwargs['port'])
        self.ehb_host = '{}:{}'.format(kwargs['edgehb_host'],kwargs['edgehb_port'])
        self.ehb_auth = aiohttp.BasicAuth(kwargs['edgehb_login'], kwargs['edgehb_passwd'])
        self.action_id = None
        self.lock_keeper = lock_keeper

        self.result_callback = result_callback
        self.step_callback = step_callback

        self.dl_dir = Path.joinpath(Path.home(), 'BUNDLE')
        Path(self.dl_dir).mkdir(parents=True, exist_ok=True)

        self.dl_filename = ''
        self.service_dir = Path.joinpath(Path.home(), '.config/systemd/user')
        Path(self.service_dir).mkdir(parents=True, exist_ok=True)
        self.auth_token = ''
        self.controller_id = kwargs['controller_id']
        self.mi = MIClient(session, **kwargs)
        self.ddi = None

    async def run_ddi(self):
        '''
        Register target on server using MI
        and switch to DDI with received parmeters
        '''
        self.logger.info('Register target on server...')
        print("register target on server")
        ''' loop until target will be registred on server'''
        while True:
            target = await self.get_target_details()
            if target:
                break
            await self.mi.register_target()

        self.logger.info('Target {} succeessfully registered.'.format(target['name']))
        self.logger.debug('name: \n {}'.format(target['name']))
        self.logger.debug('controller_id: \n {}'.format(target['controllerId']))
        self.logger.debug('securityTocken: \n {}'.format(target['securityToken']))

        '''
        get generated on server auth tocken,
        add it to config dictionary
        and run DDI with full set of params
        '''
        self.config['auth_token'] = target['securityToken']
        self.logger.debug('{}'.format(self.config))
        self.ddi = DDIClient(self.session, **self.config)

    async def get_target_details(self):
        '''
        If target exists return details (dict)
        If not return None
        '''
        #self.logger.info('')

        targets = await self.mi()
        content = targets['content']
        self.logger.debug(content)
        if content:
            for item in content:
                self.logger.debug('content item: {}'.format(item))
                if item['controllerId'] == self.controller_id:
                    return item
        self.logger.debug('no content')
        return None

    async def start_polling(self, wait_on_error=60):
        """
        Wrapper around self.poll_base_resource() for exception handling.
        """
        self.logger.info('Polling started.')

        INFO_POLLING = 'Polling cancelled'
        WARN_TIMEOUT = 'Polling failed due to TimeoutError'
        WARN_TEMP_ERROR = 'Polling failed with a temporary error:'
        WARN_EXCEPTION = 'Polling failed with an unexpected exception:'
        INFO_RETRY_FMT = 'Retry will happen in {} seconds'

        while True:
            try:
                await self.poll_base_resource()
            except asyncio.CancelledError:
                self.logger.debug(INFO_POLLING)
                break

            except asyncio.TimeoutError:
                self.logger.warning(WARN_TIMEOUT)

            except (APIError,
                    TimeoutError,
                    ClientOSError,
                    ClientResponseError) as e:
                # log error and start all over again
                self.logger.warning('{} {}'.format(WARN_TEMP_ERROR, e))

            except Exception:
                self.logger.exception(WARN_EXCEPTION)

            self.action_id = None
            self.logger.debug(INFO_RETRY_FMT.format(wait_on_error))

            await asyncio.sleep(wait_on_error)

    async def poll_base_resource(self):
        """
        Poll DDI API base resource.
        """
        while True:

            base = await self.ddi()
            if '_links' in base:

                if 'configData' in base['_links']:
                    await self.identify(base)

                if 'deploymentBase' in base['_links']:
                    await self.process_deployment(base)

                if 'cancelAction' in base['_links']:
                    await self.cancel(base)

            await self.sleep(base)

    async def identify(self, base):
        """
        Identify target against HawkBit.
        """
        self.logger.debug('Identify target against Hawkbit...')

        await self.ddi.configData(
                ConfigStatusExecution.closed,
                ConfigStatusResult.success, **self.attributes)

    async def process_deployment(self, base):
        """
        Check and download deployments
        """
        self.logger.debug('> process_deployment')

        '''disabled action_id check to enable multiple deployment of the same distro''' 
        #if self.action_id is not None:
        #    self.logger.info('Deployment is already in progress')
        #    return

        # retrieve action id and resource parameter from URL
        deployment = base['_links']['deploymentBase']['href']
        self.logger.debug('deploymentBase: {}'.format(deployment))
        match = re.search('/deploymentBase/(.+)\?c=(.+)$', deployment)
        action_id, resource = match.groups()
        self.logger.debug('action_id: {}'.format(action_id))
        self.logger.debug('resource: {}'.format(resource))
        self.logger.info('Deployment found for this target')
        # fetch deployment information
        deploy_info = await self.ddi.deploymentBase[action_id](resource)

        #get list of apps (chunks = apps)
        apps = deploy_info['deployment']['chunks']
        apps_total = len(apps)
        modules = []
        for i in range(1, apps_total + 1):
            #process deployment for each app
            self.logger.info("Pushing App {0} of {1}".format(i, apps_total))
            try:
                app = deploy_info['deployment']['chunks'][i-1]
            except IndexError:
                # send negative feedback to HawkBit
                status_execution = DeploymentStatusExecution.closed
                status_result = DeploymentStatusResult.failure
                msg = 'Deployment without chunks found. Ignoring'
                await self.ddi.deploymentBase[action_id].feedback(
                        status_execution, status_result, [msg])
                raise APIError(msg)

            try:
                artifact = app['artifacts'][0]
                self.logger.debug('artifact: {}'.format(artifact))

            except IndexError:
                # send negative feedback to HawkBit
                status_execution = DeploymentStatusExecution.closed
                status_result = DeploymentStatusResult.failure
                msg = 'Deployment without artifacts found. Ignoring'

            # prefer https ('download') over http ('download-http')
            # HawkBit provides either only https, only http or both
            if 'download' in artifact['_links']:
                download_url = artifact['_links']['download']['href']
                self.logger.debug('download url: {}'.format(download_url))

            else:
                download_url = artifact['_links']['download-http']['href']
                self.logger.debug('download url: {}'.format(download_url))

            # download artifact, check md5 and report feedback
            md5_hash = artifact['hashes']['md5']
            self.logger.info('Downloading bundle...')
            await self.download_artifact(action_id, download_url, md5_hash)
            
            # download successful, start install
            self.logger.info('Starting push...')
            try:
                self.action_id = action_id
                module_id = await self.push_to_hawkbit()
                modules.append({"id": module_id})
            except Exception as e:
                # send negative feedback to HawkBit
                status_execution = DeploymentStatusExecution.closed
                status_result = DeploymentStatusResult.failure
                await self.ddi.deploymentBase[action_id].feedback(
                        status_execution, status_result, [str(e)])
                raise APIError(str(e))
        print(modules)
        print("Creating Distributionset...")
        now = datetime.now()
        dist_name = "testDistro" + str(now.strftime("_%d%m%Y-%H%M%S"))
        dist_data = [{
            "requiredMigrationStep": False,
            "name": dist_name,
            "description": "test",
            "type": "app",
            "version": "one",
            "modules": modules
        }]
        
        await self.post_resource('/rest/v1/distributionsets/', dist_data)
        status_execution = DeploymentStatusExecution.closed
        status_result = DeploymentStatusResult.success
        #call install complete
        await self.ddi.deploymentBase[self.action_id].feedback(
                status_execution, status_result, ['Install completed'])

    async def post_resource(self, api_path, data):
        """
        Helper method for HTTP POST API requests.
        """
        post_headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }

        url = self.build_api_url(api_path)
        self.logger.debug('POST {}'.format(url))

        async with self.session.post(url, headers=post_headers,
                                     data=json.dumps(data),
                                     auth=self.ehb_auth,
                                     timeout=ClientTimeout(self.timeout)) as resp:
            await self.check_http_status(resp)
            body = await resp.json()
            self.logger.debug(body)
            return body

    async def post_artifact(self, api_path, data):
        """
        Method for uploading manifest.json file to hawkbit software module
        """
        #headers have to be empty so the boundary will be generated automatically
        post_headers = {}
        #create multipart post request
        mpwriter = MultipartWriter('form-data')
        mpp2 = mpwriter.append(json.dumps(data).encode('UTF-8'), {'CONTENT-TYPE': 'application/json'})
        mpp2.set_content_disposition('form-data', name='file', filename='manifest.json')
        
        url = self.build_api_url(api_path)
        
        self.logger.debug('POST {}'.format(url))
        async with self.session.post(url, headers=post_headers,
                                     data=mpwriter,
                                     auth=self.ehb_auth,
                                     timeout=ClientTimeout(self.timeout)) as resp:
            await self.check_http_status(resp)
            body = await resp.json()
            self.logger.debug(body)
            return body

    async def check_http_status(self, resp):
        """Log API error message."""

        #self.logger.info('')
        if resp.status not in [200, 201]:
            error_description = await resp.text()
            if error_description:
                self.logger.debug('API error: {}'.format(error_description))

            reason = resp.reason

            raise APIError('{status}: {reason}'.format(
                status=resp.status, reason=reason))

    def build_api_url(self, api_path):
        """
        Build the actual API URL.
        """
        protocol = 'https' if self.ehb_ssl else 'http'
        return '{protocol}://{host}{api_path}'.format(
            protocol=protocol, host=self.ehb_host, api_path=api_path)

    async def push_to_hawkbit(self):
        #parse manifest and get parameters
        manifest_file_name = Path(self.dl_dir).joinpath(self.dl_filename)
        manifest = {}

        with open(manifest_file_name, "r") as manifest_file:
            manifest = json.load(manifest_file)
            artifact_data = manifest_file.read()
        now = datetime.now()

        display_name = manifest['displayName'] + str(now.strftime("_%d%m%Y-%H%M%S.%f"))
        vendor_name = manifest['vendorDetail']['name']
        description = manifest['appDescription']
        _type = "application"
        version = "1.0"

        data = [
            {
                "vendor" : vendor_name,
                "name" : display_name,
                "description" : description,
                "type" : _type,
                "version" : version
            }
        ]
        #create new software module
        self.logger.info("Creating software module...")
        body = await self.post_resource('/rest/v1/softwaremodules', data)
        module_id = body[0]['id']
        #get response and id from it
        self.logger.debug("Module id is {}".format(module_id))
        self.logger.info("Uploading artifact...")
        await self.post_artifact('/rest/v1/softwaremodules/{}/artifacts'.format(module_id), manifest)
        return module_id

    async def download_artifact(self, action_id, url, md5sum,
                                tries=3):
        """
        Download bundle artifact.
        """
        #self.logger.info('')

        ERR_CHECKSUMM_FMT = 'Checksum does not match. {} tries remaining'
        STATUS_MSG_FMT = 'Artifact checksum does not match after {} tries.'

        try:
            match = re.search('/softwaremodules/(.+)/artifacts/(.+)$', url)
            software_module, self.dl_filename = match.groups()
            static_api_url = False

        except AttributeError:
            static_api_url = True

        if self.step_callback:
            self.step_callback(0, "Downloading manifest...")
        
        self.dl_filename = 'manifest.json' #IDEA: dl_filename variable as function parameter

        self.logger.debug('dl_filename: {}'.format(self.dl_filename))
        self.logger.debug('dl_dir: {}'.format(self.dl_dir))

        dl_location = Path(self.dl_dir).joinpath(self.dl_filename)

        # try several times
        for dl_try in range(tries):

            if not static_api_url:
                checksum = await self.ddi.softwaremodules[software_module].artifacts[self.dl_filename](dl_location)

            else:
                # API implementations might return static URLs, so bypass API
                # methods and download bundle anyway
                checksum = await self.ddi.get_binary(url, dl_location)

            if checksum == md5sum:
                self.logger.debug('Download manifest completed.')
                return

            else:
                self.logger.error(ERR_CHECKSUMM.format(tries-dl_try))

        # MD5 comparison unsuccessful, send negative feedback to HawkBit
        status_msg = STATUS_MSG_FMT.format(tries)
        status_execution = DeploymentStatusExecution.closed
        status_result = DeploymentStatusResult.failure

        self.logger.warning('Feedback failure')
        await self.ddi.deploymentBase[action_id].feedback(
                status_execution, status_result, [status_msg])

        raise APIError(status_msg)

    def identify_artifact(self):
        '''
        Get archive's file list and determines type of content.
        '''
        #self.logger.info('')

        dl_location = Path(self.dl_dir).joinpath(self.dl_filename)

        with tarfile.open(dl_location,'r') as tar:
            names = tar.getnames()

        items = [Path(name).name for name in names]

        if 'manifest.json' in items:
            result = 'docker'

        if 'setup.py' in items:
            result = 'python'

        self.logger.debug('{} is a |{}|'.format(self.dl_filename, result))
        return result

    async def sleep(self, base):
        """
        Sleep time suggested by HawkBit.
        """
        #self.logger.info('')
        sleep_str = base['config']['polling']['sleep']
        sleep_time = 5
        self.logger.info('Will sleep for {} seconds.'.format(sleep_time))
        t = datetime.strptime(sleep_str, '%H:%M:%S')
        delta = timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
        # await asyncio.sleep(delta.total_seconds())
        await asyncio.sleep(sleep_time)

    def create_service_file(self,
                            service_file_name,
                            exec_name,
                            description='hbloader test service'):
        '''
        Create .service file for installed programm.

        '''
        self.logger.info('> create_service_file {}'.format(self.run_mode))

        result = subprocess.run(['whereis', exec_name],  stdout=subprocess.PIPE)
        full_exec_name = result.stdout.split()[1].decode('utf-8')
        str = '''[Unit]
Description={}

[Service]
ExecStart=/usr/bin/python3 {}

[Install]
WantedBy=multi-user.target
'''.format(description, full_exec_name)
        p = Path.joinpath(self.dl_dir, service_file_name)
        with p.open( 'w') as service_file:
            service_file.write(str)
