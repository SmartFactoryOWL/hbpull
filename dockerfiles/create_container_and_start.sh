#!/bin/bash
docker create --name hbloader -v /var/run/docker.sock:/var/run/docker.sock -v /home/root/.docker/:/root/.docker/ smartfactoryowl/hbloader:0.4
docker cp hblcfg.json hbloader:/hbloader/hblcfg.json
docker start hbloader